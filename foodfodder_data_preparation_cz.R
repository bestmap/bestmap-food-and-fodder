###############################

# Title:    foodfodder_data_preparation_cz.R
# Purpose:  Moravia CS LPIS data exploration and preparation for FoodFodderModel
# Author:   Katharina Schneider
# Date:     last modified on 23 June 2022

###############################

library(sf)
library(rgdal)
library(raster)
library(rgeos)
library(ggplot2)
library(dplyr)
library(tidyverse)
library(stars)

# set Working directory
setwd("~/Bestmap/FoodFodderCZ")

# load script with functions
source("foodfodder_functions_cz.R")

# load and merge LPIS data for 2019
lpis <- st_read("CZ_LPIS_2015-19/DPB_merge_2019.shp")
lpis.df <- read.table("CZ_LPIS_2015-19/DPB_merge_2019_df.txt", header=T)
lpis <- merge(lpis, lpis.df, by="row.names")

# load LPIS data starting with the latest year
lpis <- list(lpis,
            st_read("CZ_LPIS_2015-19/DPB_merge_2018.shp"),
            st_read("CZ_LPIS_2015-19/DPB_merge_2017.shp"),
            st_read("CZ_LPIS_2015-19/DPB_merge_2016.shp"),
            st_read("CZ_LPIS_2015-19/DPB_merge_2015.shp"))

# save coordinate reference system of IACS data to variable
crs <- crs(lpis[[1]])

# load file containing crop classification
crops <- read.csv("crop_grouping_cz.csv", header = T)

# load, convert and transform ESDB raster on soil texture
soiltxt = RastertoSF("ESDB_SoilTexture_CZ.tif")
soiltxt <- st_transform(soiltxt, crs)

# create df to convert soiltxt values to soil texture strings
soiltxt.val = data.frame(values=sort(unique(soiltxt$ESDB_SoilTexture_CZ.tif)),
                         text=c("coarse", "medium", "fine", NA))

# load prepared grassland yield table
gl.yield <- st_read("grassland_yield.shp")
gl.yield <- st_transform(gl.yield, crs)

# load file with Eurostat SOC classification
soc <- read.table("CZ_SO_SGM_coefficients_20210930.txt", header=T)
soc <- soc[soc$YEAR=="2013",] #latest year
soc <- soc[soc$UNIT=="EUR_per_ha",]

#load and reproject NUTS regions shp
nuts <- st_read("NUTS_RG_01M_2016_4326.shp/NUTS_RG_01M_2016_4326.shp")
nuts <- st_transform(nuts, crs)
nuts <- nuts[nuts$LEVL_CODE == 3 & nuts$CNTR_CODE == "CZ",]


##### Geospatial Operations #####

# add year to each lpis data frame
years <- c(2019, 2018, 2017, 2016, 2015)
lpis <- lapply(seq_along(lpis), function(i) transform(lpis[[i]], year = years[i]))

# loop trough all IACS years
for ( x in seq_along(lpis)) {
  
  lp <- lpis[[x]]
  year <- unique(lp$year)
  
  # group crops
  # cultures
  lp$culture_en <- NA
  lp$culture_en[lp$KULTURA==2] <- "other arable crops"
  lp$culture_en[lp$KULTURA==4] <- "vineyard"
  lp$culture_en[lp$KULTURA==6] <- "orchard"
  lp$culture_en[lp$KULTURA==7] <- "permanent grassland"
  lp$culture_en[lp$KULTURA==9] <- "other cultures"
  lp$culture_en[lp$KULTURA==11] <- "grass and clover"
  lp$culture_en[lp$KULTURA %in% c(10,12)] <- "set-aside"
  lp$culture_en[lp$KULTURA %in% c(5,91,98,99)] <- "permanent crops"
  

  # variables for all crops listed in data frame of each year
  cropnames2019 <- c("Batty","Benedikt_l","Br_vlas","Bob","Brambory_k","Brambory_p","Brambory_s","Brokolice","Celer_bulv","Celer_rap","Cesnek_ja","Cesnek_oz","Chrpa_modr","Cibule_jar","Cibule_z_1","Cibule_zim","Cicorka","Cirok","Cizrna","Cukrovka","Docasne","Fazol__jin","Fazol_zahr","Fenykl_obe","Hermnek","Horcic_1","Horcice","Hrch__ji","Hrch_z_1","Hrch_zah","Jahodnk","Jecmen_ja","Jecmen_oz","Jestrabin","Jestrab_1","Jetel","Jetelotrav","Jlek_mno","Jlek_vyt","Jitrocel_k","Kapusta_hl","Kapusta_kr","Kedluben","Kmn_ko_1","Kmn_kor","Komonice","Konop__","Konop__1","Konop__2","Konop__3","Kopr_vonn","Kopr_vonnr","Koriandr_s","Kostrava","Kozinec","Kukuric_1","Kukuric_2","Kukurice","Kvetk","Kvetiny","Lciv","Len_olejn","Lesknice_k","Levandule","Lilek_vejc","Lnicka_se","Lupina_b","Lupina__ji","Lupina_z","Mk_set","Mangold","Mta_pepr","Medunka_l","Merlk_c","Meloun_vod","Mescek","Mrkev","Okurka_nak","Okurka_sal","Orn_bez","Ostatn_s","Ostropest","Oves","Paprika","Pastink","Pazitka","Peluska_j","Peluska_z","Petrzel_k","Petrzel_n","Pskavice","Pohanka_ob","Pr_jarno","Proso_set","Proso_setb","Psenice_1","Psenice_j","Psenice_o","Psenice__","Rajce","Redkev__j","Redkev_ol","Redkev_se","Redkvick","Repa_krmn","Repa_saln","Repice_oz","Repka_jar","Repka_ozi","Rericha","Salt_hli","Salt__ji","Salt_lis","Salt_ri","Slz_maur","Slunecnic","Slunecn_1","Smes_krmn","Smes_pro","Smes_pr_1","Smes_pr_2","Smes_pr_3","Smes_s_v3","Smes_zlep","Smesi_b","Smesi_P_1","Smesi_P_2","Smesi_PVN","Smesi_s_p","Smesi_tra","Smesky_ob","Sja","Srha_lalo","Strovnj","Svazenka_v","Svetlice","Salotka","Spent_j","Tolice__ji","Tolice_de","Topinambur","Trvy","Trvy_ce","Trvy_na","Trvy_pro","Tritikale","Tritikal_1","Trval_tr","Tykev__jin","Tykev_mus","Tykev_obec","Tykev_pomc","Tykev_velk","hor","rocnk","Vcelnk","Vicenec","Vikev_hun","Vikev__jin","Vikev_pano","Vikev_set","Vikev_seto","Vojteska","Vojtesko","Yzop_lka","Zelenina__","Zel_hl","Zel_peki","Zito_jarn","Zito_ozim","Zito_trsn")
  cropnames2018 <- c("Batty","Bazalka_vo","Benedikt_l","Bob","Brambory_k","Brambory_p","Brambory_s","Brokolice","Celer_bulv","Celer_rap","Cesnek_ja","Cesnek_oz","Chrpa_modr","Cibule_jar","Cibule_zim","Cicorka","Cirok","Cizrna","Cocka","Cukrovka","Dobromysl","Docasne","Fazol__jin","Fazol_zahr","Fenykl_obe","Fenykl_sla","Hermnek","Horcic_1","Horcice","Hrch__ji","Hrch_zah","Hrch_z_1","Hrachor","Jahodnk", "Jecmen_ja","Jecmen_oz","Jestrabin","Jestrab_1","Jetel","Jetelotrav","Jlek_mno","Jlek_vyt","Jitrocel_k","Kapusta_hl","Kapusta_ka","Kapusta_kr","Kapusta_rr","Kedluben","Kmn_kor","Komonice","Konop__","Konop__1","Konop__2","Konop__3","Konop__4","Kopr_vonn","Koriandr_s","Kostrava","Kozlcek","Kukurice","Kukuric_1","Kukuric_2","Kvetk","Kvetiny","Lciv","Len_olejn","Lesknice_k","Levandule","Libecek_l","Lilek_vejc","Lnicka_se","Lupina__ji","Lupina_b","Lupina_z","Mk_set","Mangold","Mta_pepr","Medunka_l","Meloun_vod","Mescek","Mrkev","Okurka_nak","Okurka_sal","Orn_bez","Ostatn_s","Ostropest","Oves","Paprika","Pastink","Pazitka","Peluska_j","Peluska_z","Pelynek_k","Petrzel_k","Petrzel_n","Pskavice","Pohanka_ob","Pr_jarno","Pr_ozimo","Proso_set","Psenice_j","Psenice_1","Psenice_o","Psenice__","Ptac_no","Rajce","Rebrc","Redkev__j","Redkev_ol","Redkvick","Repa_krmn","Repa_saln","Repice_oz","Repka_jar","Repka_ozi","Rericha","Salt_hli","Salt__ji","Salt_lis","Salt_ri","Saturejka","Saturejk_1","Slz_maur","Slz_pre","Slunecnic","Slunecn_1","Smes_krmn","Smes_pro","Smes_pr_1","Smes_pr_2","Smes_pr_3","Smes_s_v3","Smes_zlep","Smesi_b","Smesi_PVN","Smesi_P_1","Smesi_s_p","Smesi_tra","Sja","Srha_lalo","Svazenka_v","Svetlice","Salotka","Salvej_l","Spent_j","Spent_o","Strovno","Tolice__ji","Tolice_de","Topinambur","Topolovka","Trvy","Trvy_ce","Trvy_na","Trvy_pro","Trapatka","Tritikale","Tritikal_1","Turn","Tykev__jin","Tykev_mus","Tykev_obec","Tykev_pomc","Tykev_velk","Tymin_ob","hor","rocnk","Vcelnk","Vicenec","Vikev__jin","Vikev_pano","Vikev_setn","Vikev_seto","Vojteska","Vojtesko","Yzop_lka","Zelenina__","Zel_hl","Zel_peki","Zito_jarn","Zito_ozim")
  cropnames2017 <- c("Bob","Brambory_k","Brambory_p","Brambory_s","Brokolice","Cekanka_s","Celer_bulv","Celer_rap","Cesnek_ja","Cesnek_oz","Chrpa_modr","Cibule_jar","Cibule_z_1","Cibule_zim","Cicorka","Cirok","Cizrna","Cukrovka","Dobromysl","Docasne","Fazol__jin","Fazol_zahr","Fenykl_obe","Fenykl_sla","Hermnek","Horcice","Horcic_1","Horcic_2","Hrch__ji","Hrch_zah","Hrch_z_1","Hrachor","Jahodnk","Jecmen_ja","Jecmen_oz","Jestrabin","Jestrab_1","Jetel","Jetelotrav","Jlek_mno","Jlek_vyt","Jitrocel_k","Kapusta_hl","Kapusta_kr","Kapusta_rr","Kedluben","Kmn_kor","Kmn_ko_1","Komonice","Konop__","Konop__1","Konop__2","Konop__3","Kopr_vonn","Koriandr_s","Kostrava","Kukurice","Kukuric_1","Kukuric_2","Kvetk","Laskavec_z","Lciv","Len_olejn","Len_olejnz","Lesknice_k","Leuzea_par","Levandule","Libecek_l","Lilek_vejc","Lnicka_se","Lnicka__1","Lupina__ji","Lupina_b","Lupina_z","Mk_set","Mta_pepr","Medunka_l","Meloun_vod","Mescek","Mrkev","Neurcen","Okurka_nak","Okurka_sal","Orn_bez","Ostatn_s","Ostropest","Oves","Paprika","Pazitka","Peluska_j","Peluska_z","Pelynek_k","Petrzel_n","Petrzel_k","Pskavice","Pohanka_ob","Pr_jarno","Proskurn","Proso_set","Psenice_j","Psenice_o","Psenice__","Psenice_1","Rajce","Redkev__j","Redkev_ol","Redkev_se","Redkvick","Repa_krmn","Repa_saln","Repice_oz","Repka_jar","Repka_ozi","Rericha","Salt_hli","Salt__ji","Salt_lis","Salt_ozi","Salt_rz","Saturejka","Slz__jin","Slz_maur","Slz_pre","Slunecnic","Slunecn_1","Smes_krmn","Smes_pro","Smes_pr_1","Smes_pr_2","Smes_s_v2","Smesi_b","Smesi_plo","Smesi_s_p","Sja","Smes_zlep","Smesi_PVN","Smesi_P_1","Svazenka_v","Svetlice","Salotka","Salvej_l","Spent_j","Strovnj","Stovk","Tolice__ji","Tolice_de","Topinambur","Trvy","Trvy_ce","Trvy_na","Trvy_pro","Trapatka","Tritikale","Tritikal_1","Turn","Tykev__jin","Tykev_mus","Tykev_obec","Tykev_pomc","Tykev_velk","hor","rocnk","Vcelnk","Vicenec","Vikev__jin","Vikev_hun","Vikev_pano","Vikev_set","Vikev_seto","Vodnice","Vojteska","Vojtesko","Zelenina__","Zel_hl","Zito_ozim")
  cropnames2016 <- c("Bedrnk_a","Bob","Brambory_k","Brambory_p","Brambory_s","Brokolice","Cekanka_s","Celer_bulv","Celer_rap","Cesnek_ja","Cesnek_oz","Chrpa_modr","Cibule_jar","Cibule_zim","Cibule_z_1","Cicorka","Cirok","Cizrna","Cukrovka","Docasne","Fazol__jin","Fazol_zahr","Fenykl_obe","Fenykl_sla","Hermnek","Horcice","Horcic_1","Horcic_2","Hrch__ji","Hrch_zah","Hrch_z_1","Hrachor","Jahodnk","Jecmen_ja","Jecmen_oz","Jestrabin","Jestrab_1","Jetel","Jetelotrav","Jlek_mno","Jlek_vyt","Jitrocel_k","Kapusta_hl","Kapusta_kr","Kapusta_rr","Kedluben","Kmn_kor","Konop__","Konop__1","Konop__2","Konop__3","Konop__4","Kopr_vonn","Kopr_vonnl","Koriandr_s","Kostrava","Kozlcek","Kukurice","Kukuric_1","Kukuric_2","Kvetk","Len_olejn","Laskavec_z","Len_olejnz","Lesknice_k","Levandule","Lilek_vejc","Lnicka_se","Lupina__ji","Lupina_b","Lupina_z","Mk_set","Medunka_l","Meloun_cuk","Meloun_vod","Mescek","Mrkev","Neurcen","Okurka_nak","Okurka_sal","Orn_bez","Ostatn_s","Ostropest","Oves","Paprika","Pazitka","Peluska_j","Peluska_z","Petrzel_k","Redkev_ol","Redkev_se","Petrzel_n","Pskavice","Pohanka_ob","Pr_jarno","Pr_ozimo","Proskurn","Proso_set","Psenice_j","Psenice_o","Rajce","Redkvick","Repa_krmn","Repa_saln","Repice_oz","Repka_jar","Repka_ozi","Rericha","Salt__ji","Salt_hli","Salt_lis","Salt_ri","Saturejka","Slz__jin","Slz_maur","Slivon_s","Slunecnic","Slunecn_1","Smes_krmn","Smes_pro","Smes_pr_1","Smes_pr_2","Smes_s_v2","Smes_zlep","Smesi_b","Smesi_P_1","Smesi_PVN","Smesi_s_p","Smesi_tra","Sja","Svazenka_v","Svetlice","Salvej_l","Spent_j","Strovnj","Stovk","Tolice__ji","Tolice_de","Topinambur","Trvy","Tritikale","Tritikal_1","Trval_tr","Tykev__jin","Tykev_mus","Tykev_obec","Tykev_pomc","Tykev_velk","hor","Vcelnk","Vicenec","Vikev__jin","Vikev_setn","Vojteska","Vojtesko","Yzop_lka","Zelenina__","Zel_hl","Zel_peki","Zito_jarn","Zito_ozim")
  cropnames2015 <- c("Angrest","Bazalka_vo","Benedikt_l","Bez_cern","Bob","Brambory_k","Brambory_p","Brambory_s","Brokolice","Broskvon","Cekanka_s","Celer_bulv","Celer_rap","Cesnek_ja","Cesnek_oz","Chrpa_modr","Cibule_jar","Cibule_zim","Cibule_z_1","Cicorka","Cirok","Cizrna","Cukrovka","Dobromysl","Docasne","Drn__st","Fazol__jin","Fazol_zahr","Fenykl_obe","Horcice","Horcic_1","Hrch__ji","Hrch_zah","Hrch_z_1","Hrusen","Jablon","Jahodnk","Jecmen_ja","Jecmen_oz","Jerb_c","Jerb_ob","Jerb_os","Jestrabin","Jetel","Jetelotrav","Jlek_vyt","Jitrocel_k","Kapusta_hl","Kapusta_kr","Kastanovn","Kdoulon_p","Kedluben","Kmn_kor","Komonice","Konop__","Konop__F","Konop__S","Konop__1","Kopr_vonn","Koriandr_s","Kozlcek","Kukurice","Kukuric_1","Kukuric_2","Kvetk","Laskavec_z","Lciv","Len_olejn","Len_olejnz","Lesknice_k","Levandule","Libecek_l","Lilek_vejc","Lska_obe","Lnicka_se","Lupina__ji","Majornka","Mk_set","Malink","Mandlon_p","Mta_pepr","Materdo","Medunka_l","Meloun_vod","Merunka","Mescek","Mrkev","Neurcen","Okurka_nak","Okurka_sal","Orn_bez","Oresk","Ostatn_s","Ostropest","Oves","Paprika","Pastink","Ovoc_drev","Ovoc_dr_1","Pazitka","Peluska_j","Peluska_z","Petrzel_k","Petrzel_n","Pskavice","Pohanka_ob","Pr_jarno","Proso_set","Psenice_j","Psenice_o","Rajce","Rakytnk","Redkev_ol","Redkev_se","Redkvick","Repa_krmn","Repa_saln","Repice_oz","Repka_jar","Repka_ozi","Rericha","Rybz_ce","Rybz_c_","Rybz_c1","Salt_hli","Salt__ji","Salt_lis","Salt_ri","Saturejka","Slz__jin","Slz_maur","Slivon_ob","Slivon_re","Slivon_s","Slunecnic","Smes_krmn","Smes_pro","Smes_pr_1","Smes_pr_2","Smesi_b","Smesi_PVN","Smesi_s_p","Sja","Svazenka_v","Svetlice","Salotka","Salvej_l","Spent_j","Strovnj","Tolice_de","Topinambur","Topol_cer","Topol_kana","Topol_Maxi","Topol_Ma_1","Topol_vzne","Trvy","Tritikale","Tritikal_1","Trval_tr","Trapatka","Tresen","Tykev__jin","Tykev_mus","Tykev_obec","Tykev_pomc","hor_bez","hor_s_po","Vikev__jin","Visen","Vojteska","Vojtesko","Vrba_bl","Zel_hl","Zel_peki","Zito_jarn","Zito_ozim")
  
  # separate field blocks by crop, translate crops into english and group them
  # this has to be done for every year separately, because the number of crops and therefore the number of columns differ
  if(all(get(paste0("cropnames", year)) %in% colnames(lp)) == TRUE) {

    lp.long = ColumnsToRows(lp, c("NKOD_DPB",get(paste0("cropnames", year))), get(paste0("cropnames", year)), "crop_cz", "crop_area")

    lp.long <- merge(lp.long, crops, by="crop_cz", all.x=T)

    lp.agg <- aggregate(crop_area ~ NKOD_DPB+group+crop_eng, data=lp.long, sum)
    lp <- merge(lp, lp.agg, by="NKOD_DPB", all.x=T)

  } else {

    print("crops in data frame do not fully match the values of the cropnames vector")
  }
  
  # when culture is 'other arable crops' fill in the crop name
  lp$crop <- ifelse(lp$culture_en == "other arable crops", lp$group, lp$culture_en)
  
  # match crop names to SOC names
  lp$crop_soc <- NA
  lp$crop_soc[lp$crop=="barley"] <- "Barley"
  lp$crop_soc[lp$crop=="grass and clover"] <- "Forage plants - temporary grass"
  lp$crop_soc[lp$crop=="other arable crops"] <- "Aromatic, medicinal and culinary plants"
  lp$crop_soc[lp$crop_en %in% c("other","temporarily","mixture","unspecified","buckwheat","amaranth")] <- "Other arable land crops" # value is 0!
  lp$crop_soc[lp$crop_en=="soy"] <- "Soya" # value is 0!
  lp$crop_soc[lp$crop_en=="flowers"] <- "Flowers - under glass" # outdoor value is 0!
  lp$crop_soc[lp$crop_en %in% c("poppy seeds","linen")] <- "Other oil seed crops"
  lp$crop_soc[lp$crop=="permanent crops"] <- "Other permanent crops" # value is 0!
  lp$crop_soc[lp$crop_en=="raspberry"] <- "Berry species"
  lp$crop_soc[lp$crop_en=="fodder"] <- "Forage plants - other green fodder - other than green maize"
  lp$crop_soc[lp$crop=="legumes"] <- "Peas, field beans and sweet lupines" # value is 0!
  lp$crop_soc[lp$crop_en %in% c("vetch", "lentils", "legume")] <- "Pulses other than peas, field beans and sweet lupines"
  lp$crop_soc[lp$crop=="maize"] <- "Grain maize"
  lp$crop_soc[lp$crop=="oats"] <- "Oats"
  lp$crop_soc[lp$crop=="orchard"] <- "Fruit species of temperate climate zones"
  lp$crop_soc[lp$crop_en=="hemp"] <- "Hemp"
  lp$crop_soc[lp$crop_en %in% c("chestnut", "hazelnut", "almond", "walnut")] <- "Fruit and berry plantations - nuts"
  lp$crop_soc[lp$crop_en=="flax (oil)"] <- "Flax"
  lp$crop_soc[lp$crop=="vineyard"] <- "Vineyards - quality wine"
  lp$crop_soc[lp$crop=="permanent grassland"] <- "Permanent grassland and meadow - rough grazings"
  lp$crop_soc[lp$crop=="potatoes"] <- "Potatoes"
  lp$crop_soc[lp$crop=="rapeseed"] <- "Rape and turnip"
  lp$crop_soc[lp$crop=="rye"] <- "Rye"
  lp$crop_soc[lp$crop=="sugar beets"] <- "Sugar beet"
  lp$crop_soc[lp$crop=="sunflower"] <- "Sunflower"
  lp$crop_soc[lp$crop=="vegetables"] <- "Fresh vegetables, melons, strawberries"
  lp$crop_soc[lp$crop=="wheat"] <- "Common wheat and spelt"
  lp$crop_soc[lp$crop=="triticale"] <- "Other cereals"
  

  ##### AES #####
  # variable for all relevant AES
  AES_cols <- c("VYM_OP_A_1","VYM_OP_A_2","VYM_OP_A_4","VYM_OP_A_5","VYM_OP_A_9","VYM_OP__10","VYM_OP__11","VYM_OP__12","VYM_OP__13","VYM_OP__15","VYM_OP__16","VYM_OP__17","VYM_OP__18","VYM_OP__19","VYM_OP__20","VYM_OP__21","VYM_OP__22","VYM_OP__23")

  lp.long = ColumnsToRows(lp, c("NKOD_DPB",AES_cols), AES_cols, "AES_info", "AES_area")

  # group AES
  lp.long$AES_group <- NA
  lp.long$AES_group[lp.long$AES_info %in% c("VYM_OP_A_1","VYM_OP_A_2","VYM_OP_A_4","VYM_OP_A_5","VYM_OP__10","VYM_OP__11","VYM_OP__12","VYM_OP__15","VYM_OP__16","VYM_OP__17")] <- "MaintainigGrasslands"
  lp.long$AES_group[lp.long$AES_info %in% c("VYM_OP__18","VYM_OP__19","VYM_OP__20","VYM_OP__21","VYM_OP__22","VYM_OP__23")] <- "LUConvGrassland"
  lp.long$AES_group[lp.long$AES_info %in% c("VYM_OP_A_9","VYM_OP__13")] <- "BufferAreas"
  
  # add info about maintainig grassland sub schemes
  lp.long$AES_maint_gl <- NA
  lp.long$AES_maint_gl[lp.long$AES_info=="VYM_OP_A_1"] <- "VYM_OP_1"
  lp.long$AES_maint_gl[lp.long$AES_info=="VYM_OP_A_2"] <- "VYM_OP_2"
  lp.long$AES_maint_gl[lp.long$AES_info=="VYM_OP_A_4"] <- "VYM_OP_4"
  lp.long$AES_maint_gl[lp.long$AES_info=="VYM_OP_A_5"] <- "VYM_OP_5"
  lp.long$AES_maint_gl[lp.long$AES_info=="VYM_OP__10"] <- "VYM_OP_10"
  lp.long$AES_maint_gl[lp.long$AES_info=="VYM_OP__11"] <- "VYM_OP_11"
  lp.long$AES_maint_gl[lp.long$AES_info=="VYM_OP__12"] <- "VYM_OP_12"
  lp.long$AES_maint_gl[lp.long$AES_info=="VYM_OP__15"] <- "VYM_OP_15"
  lp.long$AES_maint_gl[lp.long$AES_info=="VYM_OP__16"] <- "VYM_OP_16"
  lp.long$AES_maint_gl[lp.long$AES_info=="VYM_OP__17"] <- "VYM_OP_17"

  lp.long$AES_maint_gl[is.na(lp.long$AES_maint_gl)] <- "other"
  
  lp.agg <- aggregate(AES_area ~ NKOD_DPB+AES_maint_gl+AES_group, data = lp.long, sum)
  lp.agg$AES_maint_gl[lp.agg$AES_maint_gl=="other"] <- NA
  lp <- merge(lp, lp.agg, by = "NKOD_DPB", all.x=T)
  

  ##### EFA #####
  # variable for all relevant EFA
  EFA_cols <- c("VYM_MPL","VYM_UHOM","VYM_UHOZ")

  lp.long = ColumnsToRows(lp, c("NKOD_DPB",EFA_cols), EFA_cols, "EFA_info", "EFA_area")

  # group EFA
  lp.long$EFA_group <- NA
  lp.long$EFA_group[lp.long$EFA_info %in% c("VYM_UHOM", "VYM_UHOZ")] <- "FallowLand"
  lp.long$EFA_group[lp.long$EFA_info=="VYM_MPL"] <- "CoverCrops"

  lp.agg <- aggregate(EFA_area ~ NKOD_DPB+EFA_group, data = lp.long, sum)
  lp <- merge(lp, lp.agg, by = "NKOD_DPB", all.x=T)
  

  ##### organic farms #####
  # variable for all columns with organic farming information
  ORG_cols <- c("VYM_OP_A_7","VYM_OP__14","VYM_OP__24","VYM_OP_EZ_")

  lp.long = ColumnsToRows(lp, c("NKOD_DPB",ORG_cols), ORG_cols, "organic_farm", "organic_area")

  lp.long$organic <- NA
  lp.long$organic[lp.long$organic_farm %in% c("VYM_OP_A_7","VYM_OP__14","VYM_OP__24","VYM_OP_EZ_")] <- TRUE

  lp.agg <- aggregate(organic_area ~ NKOD_DPB+organic, data = lp.long, sum)
  lp <- merge(lp, lp.agg, by = "NKOD_DPB", all.x=T)
  lp$organic[is.na(lp$organic)] <- FALSE
  
  ##### Cover Crops #####
  # create column to assign cover crop species
  lp$CovCrop <- NA
  
  # the following code will only be run for the IACS years 2016-2019
  if(x < 5){
    
    # IACS data of previous year
    lp2 <- lpis[[x+1]]
    
    # find out if cover crops were planted on the field in the previous year
    lp = CoverCropsPreviousYear(lp, lp2)
    lp$CovCrop <- ifelse(lp$CovCrop == TRUE & lp$crop %in% c("maize","wheat"), lp$CovCrop, NA)
  
  }


  # extract nuts region per field to new column of inv
  lp <- IsectNewColumn(lp, nuts, "intersect", "NUTS_NAME", "NUTS_NAME")

  
  ##### Soil Texture #####
  # extract median soil texture per field to new column of inv
  lp <- IsectNewColumn(lp, soiltxt, "join", "ESDB_SoilTexture_CZ.tif", "soiltxt", largest=T)

  # convert numeric soil texture values to strings
  lp$soiltxt <- soiltxt.val$text[match(lp$soiltxt, soiltxt.val$values)]


  ##### Baseline Yield from WOFOST #####
  # match WOFOST crop to IACS crops
  lp$crop_wofost <- crops$wofost[match(lp$crop, crops$group)]

  # select relevant WOFOST crops for CS. Here:
  # 2: maize, 6: sugar beet, 90: wheat, 93: sunflower, 94: winter rapeseed, 95: spring barley
  crops_wofost = c(2, 6, 90, 93, 94, 95)

  # loop through all crops
  for (i in 1:length(crops_wofost)){

    # load WOFOST crop map of the respective crop and year and rename columns
    r <- RastertoSF(paste0("WOFOST_CZ/Yield_out_", crops_wofost[i], "__pot_yield__", year, ".tif"))
    names(r) = c("yield", "geometry")

    # define name of new column, then extract median yield per field to new column of lp
    col = paste0("yield_", crops_wofost[i])
    lp <- IsectNewColumn(lp, r, "aggregate", "yield", col, median)
  }


  ##### grasslands #####
  N_input = c(0, 60, 120)

  for (i in 1:length(N_input)){
    grass.y <- gl.yield[,c(paste0("N",N_input[i],"kg_ha"), "geometry")]
    names(grass.y) <- c("yield", "geometry")

    col = paste0("yield_N_", N_input[i])
    lp <- IsectNewColumn(lp, grass.y, "aggregate", "yield", col, median)
  }


  lp <- as.data.frame(lp)
  
  # create yield column
  lp$yield <- NA
  
  for (i in 1:length(crops_wofost)){

    # if wofost yield is 0, change to lowest non-zero value
    col = paste0("yield_", crops_wofost[i])
    lp[, col] <- ifelse(lp[, col] == 0, sort(unique(lp[,col]))[2], lp[, col])

    # assign wofost yield to wofost crop
    lp$yield <- ifelse(lp$crop_wofost == crops_wofost[i], lp[, col], lp$yield)
  }

  for (i in 1:length(N_input)){

    # if grassland yield is NA, change to lowest non-zero value
    col = paste0("yield_N_", N_input[i])
    lp[,col] <- ifelse(is.na(lp[,col]), sort(unique(lp[,col]))[2], lp[,col])
  }
  
  # add grassland yield to yield column
  lp$yield <- ifelse(lp$crop=="permanent grassland", lp$yield_N_120, lp$yield)
  lp$yield <- ifelse(lp$crop=="grass and clover", 2*(lp$yield_N_120), lp$yield)
  
  # area column combining AES area and crop area
  lp$area <- lp$AES_area
  lp$area <- ifelse(is.na(lp$area), lp$crop_area, lp$area)
  
  # add standard output coefficient
  lp$soc_value <- soc$VALUE[match(lp$crop_soc, soc$LABEL)]
  
  
  # remove unnecessary columns
  lp <- lp[,c("NKOD_DPB", "ID_UZ", "culture_en", "year", "crop", "crop_area", "AES_group", "AES_area", "area", 
              "AES_maint_gl", "EFA_group", "organic", "soiltxt", "CovCrop", "yield_N_0", "yield_N_60",
              "yield_N_120", "crop_wofost", "yield", "NUTS_NAME", "soc_value")]
  
  # write to file
  write.table(lp, paste0(unique(lp$year), "_foodfodder_input_cz.txt"), row.names = F, sep = "\t")
  
}
