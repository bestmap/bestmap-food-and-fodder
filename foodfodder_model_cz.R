###################################################

# Title:      foodfodder_model_cz.R
# Purpose:    BESTMAP food & fodder model CZ
# Author:     Katharina Schneider
# Date:       last modified on 27 June 2022

###################################################

# load packages
library(sf)
library(rgdal)
library(raster)
library(rgeos)
library(readxl)
library(ggplot2)
library(tidyr)
library(plyr)

# set Working directory
setwd("~/Bestmap/FoodFodderCZ")

# load script with functions
source("foodfodder_functions_cz.R")

# load data preparation script
# source("foodfodder_data_preparation_cz.R")

##### Load and Prepare Data #####

# load the LPIS data 
lpis = list(read.table("2019_foodfodder_input_cz.txt", header = T, sep = "\t"),
            read.table("2018_foodfodder_input_cz.txt", header = T, sep = "\t"),
            read.table("2017_foodfodder_input_cz.txt", header = T, sep = "\t"),
            read.table("2016_foodfodder_input_cz.txt", header = T, sep = "\t"),
            read.table("2015_foodfodder_input_cz.txt", header = T, sep = "\t"))

# load and prepare SoilHealthDatabase 
# The database contains a collection of studies investigating the ffects of covr crops on main cro yield
# for further reference, please see https://github.com/jinshijian/SoilHealthDB
# and DOI:10.1016/j.still.2020.104575 

db <- read_excel("SoilHealthDB_V2.xlsx", sheet = 1); head(db)
koeppen <- read.table("Koeppen-Geiger-ASCII.txt", header=T); head(koeppen)

db <- PrepareSoilHealthDB(db, koeppen)

# calculate mean response ratio as well as uncertainty measures per group
DBmean <- ddply(db, c("koeppen", "Texture_Group", "GrainCropGroup2"), summarise,
                N    = sum(!is.na(ExperimentID)),
                RR_mean = mean(RR, na.rm=T),
                sd   = sd(RR, na.rm=T),
                se   = sd / sqrt(N),
                cv = sd / RR_mean * 100
)

DBmean$cv <- DBmean$sd / DBmean$RR * 100

# select relevant groups for CS
db1 <- DBmean[DBmean$GrainCropGroup2 %in% c("Corn","Wheat") & DBmean$koeppen=="D",]

# transform response ratios to percentage yield change
db1$Perc_mean = (exp(db1$RR_mean)-1) * 100

# repeat data preparation for group 'wheat' to avoid NA in texture group 'fine'
# calculate mean response ratio as well as uncertainty measures per group
DBmean <- ddply(db, c("koeppen", "GrainCropGroup2"), summarise,
                N    = sum(!is.na(ExperimentID)),
                RR_mean = mean(RR, na.rm=T),
                sd   = sd(RR, na.rm=T),
                se   = sd / sqrt(N),
                cv = sd / RR_mean * 100
)

DBmean$cv <- DBmean$sd / DBmean$RR * 100

# select relevant groups for CS
db2 <- DBmean[DBmean$GrainCropGroup2 %in% c("Wheat") & DBmean$koeppen=="D",]

# transform response ratios to percentage yield change
db2$Perc_mean = (exp(db2$RR_mean)-1) * 100


##### Model AES/EFA effects on food/fodder for each year #####

# create list to store aggregated results per farm
farms <- list()

for ( x in seq_along(lpis)) {
  
  lp <- lpis[[1]]
  year <- unique(lp$year)
  
  
  mean.yield <- aggregate(yield ~ crop, lp, mean, na.rm = T)
  
  lp$mean_yield <- mean.yield$yield[match(lp$crop, mean.yield$crop)]
  
  # assign grassland N input scenarios to maintaining grassland schemes
  lp$yield2 <- NA
  lp$yield2[lp$AES_maint_gl %in% c("VYM_OP_1","VYM_OP_4","VYM_OP_11","VYM_OP_17")] <- lp$yield_N_120[lp$AES_maint_gl %in% c("VYM_OP_1","VYM_OP_4","VYM_OP_11","VYM_OP_17")]
  lp$yield2[lp$AES_maint_gl %in% c("VYM_OP_2","VYM_OP_5","VYM_OP_10","VYM_OP_12","VYM_OP_16")] <- lp$yield_N_60[lp$AES_maint_gl %in% c("VYM_OP_2","VYM_OP_5","VYM_OP_10","VYM_OP_12","VYM_OP_16")]
  lp$yield2[lp$AES_maint_gl %in% "VYM_OP_15"] <- lp$yield_N_0[lp$AES_maint_gl %in% "VYM_OP_15"]
  
  
  # Default: assume yield change = 0, i.e. baseline scenario yield
  lp$Yield_Change <- 0
  
  # Effect of buffer areas
  # Yield Effect Flower Strips according to Albrecht et al. 2020
  # http://centaur.reading.ac.uk/95359/1/ele.13576.pdf
  lp$Yield_Change[lp$AES_group=="BufferAreas"] <- 0
  
  # Effect of land use conversion
  # LUConv --> no effect on yield but on cultivation area
  lp$Yield_Change[lp$AES_group %in% c("LUConvGrassland", "LUConvForest")] <- 0
  
  # Effect of cover crops
  lp$Yield_Change[lp$soiltxt == "coarse" &
                    lp$crop == "maize" &
                    lp$CovCrop %in% TRUE] <- db1$Perc_mean[which(db1$Texture_Group=="coarse" &
                                                             db1$GrainCropGroup2=="Corn")] 
  lp$Yield_Change[lp$soiltxt == "coarse" &
                    lp$crop == "wheat" & 
                    lp$CovCrop %in% TRUE] <- db1$Perc_mean[which(db1$Texture_Group=="coarse" &
                                                                   db1$GrainCropGroup2=="Wheat")]
  lp$Yield_Change[lp$soiltxt == "medium" &
                    lp$crop == "maize" & 
                    lp$CovCrop %in% TRUE] <- db1$Perc_mean[which(db1$Texture_Group=="medium" &
                                                             db1$GrainCropGroup2=="Corn")]
  lp$Yield_Change[lp$soiltxt == "medium" &
                    lp$crop == "wheat" & 
                    lp$CovCrop %in% TRUE] <- db1$Perc_mean[which(db1$Texture_Group=="medium" &
                                                             db1$GrainCropGroup2=="Corn")] 
  lp$Yield_Change[lp$soiltxt == "fine" &
                    lp$crop == "maize" & 
                    lp$CovCrop %in% TRUE] <- db1$Perc_mean[which(db1$Texture_Group=="fine" &
                                                             db1$GrainCropGroup2=="Corn")]
  lp$Yield_Change[lp$soiltxt == "fine" &
                    lp$crop == "wheat" & 
                    lp$CovCrop %in% TRUE] <- db2$Perc_mean
  
  # Effect of organic management (on top of effects of other AES)
  lp$Yield_Change[lp$organic=="TRUE"] <- lp$Yield_Change[lp$organic=="TRUE"]-36.5
  lp$Yield_Change[lp$organic=="TRUE" &
                    lp$crop %in% c("wheat","barley","rye","oats","triticale")] <- lp$Yield_Change[which(lp$organic=="TRUE" &
                                                                                                    lp$crop %in% c("wheat","barley","rye","oats","triticale"))]-36 # cereals
  lp$Yield_Change[lp$organic=="TRUE" &
                    lp$crop %in% "legumes"] <- lp$Yield_Change[which(lp$organic=="TRUE" &
                                                             lp$crop=="legumes")]-30 # legumes
  lp$Yield_Change[lp$organic=="TRUE" &
                    lp$crop %in% "potatoes"] <- lp$Yield_Change[which(lp$organic=="TRUE" &
                                                              lp$crop=="potatoes")]-41 # potatoes
  lp$Yield_Change[lp$organic=="TRUE" &
                    lp$crop %in% c("rapeseed","sunflower")] <- lp$Yield_Change[which(lp$organic=="TRUE" &
                                                                                 lp$crop %in% c("rapeseed","sunflower"))]-73 # oil crops
  lp$Yield_Change[lp$organic=="TRUE" &
                    lp$crop %in% "grass and clover"] <- lp$Yield_Change[which(lp$organic=="TRUE" &
                                                                      lp$crop=="grass and clover")]-56 # fodder crops
  
  # Calculate modeled yield
  lp$Yield_mod <- lp$yield + (lp$yield * lp$Yield_Change / 100)
  lp$Yield_mod[lp$AES_group %in% "MaintainingGrassland"] <- lp$yield2 + (lp$yield2 * lp$Yield_Change / 100)[lp$AES_group %in% "MaintainingGrassland"]
  
  #Calculate baseline and modeled production
  lp$production <- lp$yield * lp$area
  lp$production_mod <- lp$Yield_mod * lp$area
  
  # multiply by standard output coefficient
  lp$soc <- ifelse(lp$yield==0, lp$soc_value, lp$yield / lp$mean_yield * lp$soc_value)
  lp$soc_mod <- ifelse(lp$yield==0, lp$soc_value, lp$Yield_mod / lp$mean_yield * lp$soc_value)
  
  # soc for buffer areas & land use conversion to forest in no AES scenario: we assume it is equal to the average SOC of all fields
  lp$median_soc <- median(lp$soc_value, na.rm=T)
  
  # soc for land use conversion to grassland in no AES scenario: we assume it is equal to the average SOC of all arable fields
  lp.al <- lp[lp$culture_en == "other arable crops" & !is.na(lp$culture_en),]
  lp$median_soc_al <- median(lp.al$soc_value, na.rm=T) # fix it! same value!!
  
  lp$soc[lp$AES_group %in% "BufferAreas"] <- lp$median_soc[lp$AES_group %in% "BufferAreas"]
  lp$soc[lp$EFA_group %in% "LUConvForest"] <- lp$median_soc[lp$EFA_group %in% "LUConvForest"]
  lp$soc[lp$EFA_group %in% "LUConvGrassland"] <- lp$median_soc_al[lp$EFA_group %in% "LUConvGrassland"]
  
  # multiply by area to obtain standard output
  lp$so <- lp$soc * lp$area
  lp$so_mod <- lp$soc_mod * lp$area
  
  # aggregate to farm level
  frms <- ddply(lp, .(ID_UZ), summarize,
                yield = mean(yield, na.rm = T),
                Yield_mod = mean(Yield_mod, na.rm = T),
                production = sum(production, na.rm=T),
                production_mod = sum(production_mod, na.rm=T),
                so = sum(so, na.rm = T),
                so_mod = sum(so_mod, na.rm = T))
  
  write.table(frms, paste0(unique(lp$year), "_foodfodder_model_output_cz.txt"), row.names = F, sep = "\t")
  
  farms[[x]] <- frms
  
  lpis[[x]] <- lp
  
}
