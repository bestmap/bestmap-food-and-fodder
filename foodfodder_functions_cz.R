###################################################

# Title:      foodfodder_functions.R
# Purpose:    collection of functions used for running the BESTMAP food & fodder models
# Author:     Katharina Schneider
# Date:       last modified on 21 April 2022

###################################################

##### RastertoSF #####
# This function reads a raster file as a stars object and converts it to an SF object
# Arguments: filepath: path to input raster file
# Packages stars and sf are required to run this function
RastertoSF = function(filepath){
  raster = read_stars(filepath)
  sf = st_as_sf(raster)
  return(sf)
}


##### IsectNewColumn #####
# This function performs a geospatial operation (intersection, aggregation or join as indicated) 
# of an SF and another SF object or raster stores and creates a new column of the input sf object with the result
# Arguments: x: sf object, y: sf object or raster, method: "intersect", "aggregate", "join", 
# incol: column of y to perform operation on, newcol: new column to add to x with result of operation
# Package sf is required to run this function


IsectNewColumn = function(x, y, method, incol, newcol,...){
  if(method=="intersect"){
    
    x.isect <- st_intersection(x, y, ...)
    x.isect <- as.data.frame(x.isect)
    x[,newcol] <- x.isect[,incol][match(row.names(x), row.names(x.isect))]
    return(x)
    
  }else{
    if(method=="aggregate"){
      
      x.isect <- aggregate(y, x, ...)
      x.isect <- as.data.frame(x.isect)
      x[,newcol] <- x.isect[,incol][match(row.names(x), row.names(x.isect))]
      return(x)
      
    }else{
      if(method=="join"){
        
        x.isect <- st_join(x, y, ...)
        x.isect <- as.data.frame(x.isect)
        x[,newcol] <- x.isect[,incol][match(row.names(x), row.names(x.isect))]
        return(x)
        
      }else{
        
        print("no suitable method selected")
      }
    }
  }
}


##### ColumnsToRows #####
# function to convert selected columns of a data.frame of an sf object into row values
# Arguments: lp: sf object or data.frame with AECS data of year of interest, selcol: columns to select from data.frame
# (must include id and all columns to perform function on), valcol: columns from data.frame to perform function on,
# colname1: name of column to assign former column names to, colname2: name of column to assign values to
# Package tidyr is required to run this function

ColumnsToRows = function(lp, selcol, valcol, colname1, colname2){
  lp.wide <- lp[,selcol]
  lp.long <- pivot_longer(lp.wide, cols = c(all_of(valcol)), names_to = colname1, values_to = colname2)
  lp.long[[colname2]] [lp.long[[colname2]] == 0] <- NA
  lp.long <- na.omit(lp.long)
  
  return(lp.long)
}


#####CoverCropsPreviousYear#####
# function to identify field with EFA cover crops in the previous year and to assign the correct cover crop species
# Arguments: lp: sf object or data.frame with IACS data of year of interest, lp2: sf object or date.frame with IACS data of previous year,
# crop: crop of interest

CoverCropsPreviousYear = function(lp, lp2){
  # identify all fields with crop of interest and cover crops in the previous year
  cc <- unique(lp2$NKOD_DPB[lp2$VYM_MPL>0])
  cc <- na.omit(cc)
  cc.fields <- data.frame(NKOD_DPB = cc)
  
  lp$CovCrop[lp$NKOD_DPB %in% cc.fields$NKOD_DPB] <- TRUE
  
  return(lp)
}

##### PrepareSoilHealthDB #####
# function to prepare SoilHelathDB in order to estimate cover crop yields effects according to Jian et al. 2020
# for further reference, please see https://github.com/jinshijian/SoilHealthDB
# and DOI:10.1016/j.still.2020.104575
# Arguments: db: SoilHealthDB data.frame, kop: data.frame containing lat-lon coordinates of Koeppen-Geiger climate zones

PrepareSoilHealthDB = function(db, kop){
  # Match Koeppen-Geiger Climate Zone to Study Location
  kop$zone <- substr(kop$Cls, 1, 1)
  kop$Lat <- round(kop$Lat)
  kop$Lon <- round(kop$Lon)
  db$Latitude <- round(db$Latitude)
  db$Longitude <- round(db$Longitude)
  db$match <- paste(db$Latitude, db$Longitude)
  kop$match <- paste(kop$Lat, kop$Lon)
  db$koeppen <- kop$zone[match(db$match, kop$match)]
  
  # Calculate Response Ratio 
  db$RR = log(db$Yield_T / db$Yield_C)
  
  # Classify Soil Texture
  db$Texture_Group <- NA
  db$Texture_Group[db$Texture %in% c("Clay loam", "Silty clay loam", "Sandy clay", "Silty clay", "Clay")] <- "fine"
  db$Texture_Group[db$Texture %in% c("Loam", "Silt loam", "Silt", "Sandy clay loam")] <- "medium"
  db$Texture_Group[db$Texture %in% c("Sand", "Loamy sand", "Sandy loam", "Sandy")] <- "coarse"
  
  # Classify Cover Crop Species
  db$CoverCropGroup2 <- NA
  db$CoverCropGroup2[db$CoverCropGroup %in% c("BG", "LB", "LG", "LL", "MOT", "MTT", "AVG")] <- "mixed"
  db$CoverCropGroup2[db$CoverCropGroup %in% c("Legume")] <- "Legume"
  db$CoverCropGroup2[db$CoverCropGroup %in% c("Grass", "Rye")] <- "Grass"
  db$CoverCropGroup2[is.na(db$CoverCropGroup2)] <- "others"
  
  # Classify Cash Crops
  db$GrainCropGroup2 <- NA
  db$GrainCropGroup2[db$GrainCropGroup=="Vegetable"] <- "Vegetable"
  db$GrainCropGroup2[db$GrainCropGroup=="Soybean"] <- "Soybean"
  db$GrainCropGroup2[db$GrainCropGroup=="Wheat"] <- "Wheat"
  db$GrainCropGroup2[db$GrainCropGroup %in% c("Corn", "Maize")] <- "Corn"
  db$GrainCropGroup2[db$GrainCropGroup %in% c("CSW")] <- "CSW"
  db$GrainCropGroup2[db$GrainCropGroup %in% c("CS")] <- "CS"
  db$GrainCropGroup2[is.na(db$GrainCropGroup2)] <- "other"
  
  # further aggregate due to low number of observations per group
  db$CoverCropGroup3 <- NA
  db$CoverCropGroup3 <- ifelse(db$CoverCropGroup2 == "Grass", "Grass", "others")
  
  return(db)
}
