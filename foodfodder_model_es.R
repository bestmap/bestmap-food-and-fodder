###################################################

# Title:      foodfodder_model_es.R
# Purpose:    BESTMAP food & fodder model ES
# Author:     Anne Paulus
# Date:       last modified on 30 March 2022

###################################################

# load packages
library(sf)
library(rgdal)
library(raster)
library(rgeos)
library(readxl)
library(ggplot2)
library(tidyr)
library(plyr)

# set Working directory
setwd("")

# load script with functions
source("foodfodder_functions.R")

# load data preparation script
# source("foodfodder_data_preparation_ES.R")

##### Load and Prepare Data #####

# load the DUN data 
dun <- list(read.table("2019_foodfodder_input.txt", header = T, sep="\t"),
            read.table("2018_foodfodder_input.txt", header = T, sep="\t"),
            read.table("2017_foodfodder_input.txt", header = T, sep="\t"),
            read.table("2016_foodfodder_input.txt", header = T, sep="\t"),
            read.table("2015_foodfodder_input.txt", header = T, sep="\t"))

##### Model AES/EFA effects on food/fodder for each year #####

# create list to store aggregated results per farm
farms <- list()

for ( x in seq_along(dun)) {
  
  d <- dun[[x]]
  
  d$yield[is.na(d$yield)] <- 0
  
  mean_yield <- aggregate(yield~crop, d, mean, na.rm =T)
  
  d$mean_yield <- mean_yield$yield[match(d$crop, mean_yield$crop)]
  
  # Default: assume yield change = 0, i.e. baseline scenario yield
  d$Yield_Change <- 0
  
  # Effect of maintaining grasslands
  d$Yield_Change[d$AES_EFA_group=="MaintainingGrasslands"] <- 0
  
  # Effect of fallow
  d$Yield_Change[d$AES_EFA_group=="FallowLand"] <- 0
  
  # Effect of cover crops
  d$Yield_Change[d$AES_EFA_group=="CoverCrops"] <- 0
  
  # Effect of organic farming
  d$Yield_Change[d$AES_EFA_group=="OrganicFarming"] <- - 20
  d$Yield_Change[d$AES_EFA_group=="OrganicFarming" & d$crop_soc %in% c("Olive plantations - oil production",
                   "Fruit and berry plantations - nuts", "Fruit species of temperate climate zones",
                   "Fruit species of subtropical climate zones", "Citrus plantations")] <- - 0
  
  
  # Calculate modeled yield 
  d$Yield_mod <- d$yield + (d$yield * d$Yield_Change / 100)
  
  # Calculate baseline and modeled production
  d$production_mod <- d$Yield_mod * d$HA_DEC
  
  # multiply by standard output coefficient
  d$soc <- ifelse(d$yield==0, d$soc, d$yield / d$mean_yield * d$soc)
  d$soc_mod <- ifelse(d$yield==0, d$soc, d$Yield_mod / d$mean_yield * d$soc)
  
  # multiply by area to obtain standard output
  d$so <- d$soc * d$HA_DEC
  d$so_mod <- d$soc_mod * d$HA_DEC
  

  frms <- ddply(d, .(ID_EXPLOTACIO), summarize,
                  yield = mean(yield, na.rm = T),
                  Yield_mod = mean(Yield_mod, na.rm = T),
                  production = sum(production, na.rm=T),
                  production_mod = sum(production_mod, na.rm=T),
                  so = sum(so, na.rm=T),
                  so_mod = sum(so_mod, na.rm=T))
                  
  farms[[x]] <- frms
  
  dun[[x]] <- d
  
 }

